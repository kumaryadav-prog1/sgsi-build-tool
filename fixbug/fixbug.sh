#!/bin/bash

LOCALDIR=`cd "$( dirname $0 )" && pwd`
cd $LOCALDIR

./rm.sh > /dev/null 2>&1

fix=$1

if [ $fix = "miui" ];then
 echo "Doing MIUI Fixes please wait..."
 ./miui.sh
 echo "修复完成"
 rm -rf services.jar.out/
 cd ../
else
 echo "" > /dev/null 2>&1
fi

if [ $fix = "flyme" ];then
 ./flyme.sh
 echo "修复完成"
 rm -rf services.jar.out/
 cd ../
else
 echo "" > /dev/null 2>&1
fi
 
if [ $fix = "h2os" ];then
 ./h2os.sh
 echo "修复完成"
 rm -rf services.jar.out/
 cd ../
else
 echo "" > /dev/null 2>&1
fi
 
if [ $fix = "color" ];then
 ./color.sh
 echo "修复完成"
 rm -rf services.jar.out/
 cd ../
else
 echo "" > /dev/null 2>&1
fi

